<?php
class Sesion
{
    


    public function __construct()
    {
        session_start();
    }
    //si hay datos los elimino
    function login()
    {
        if(isset($_SESSION['datos'])){
            unset($_SESSION['datos']);
        }
        require "vista.php";
    }
    
    //compruebo si existen y los meto en sesion
    function auth()
    {
        if (!empty($_POST['nombre'])) {
            setcookie("nombre", $_POST['nombre'] ,time() +  3600);
            $_SESSION['datos'][] = $_POST['nombre'];
        }else{
            header('Location: index.php?method=login');
        }
        if(!empty($_POST['apellidos'])){
            setcookie("apellidos", $_POST['apellidos'] ,time() +  3600);
            $_SESSION['datos'][] = $_POST['apellidos'];
            
        }else{
            header('Location: index.php?method=login');
        }
        if(!empty($_POST['email'])){ 
            setcookie("email", $_POST['email'] ,time() +  3600);
            $_SESSION['datos'][] = $_POST['email'];
            
        }else{
            header('Location: index.php?method=login');

        }
        
       require "vista2.php";
    }
    //si hay modulos los elimino
    function home()
    {
        if(isset($_SESSION['modulos'])){
            unset($_SESSION['modulos']);
        }
        require "vista3.php";
    }
    //añado los modulos a la sesion
    function añadirModulo()
    {
        if(isset($_POST['dec'])){
            setcookie("dec", $_POST['dec'] ,time() +  3600);
            $_SESSION['modulos'][] = "Desarrollo Web en Entorno Cliente";
        }
        if(isset($_POST['des'])){
            setcookie("des", $_POST['des'] ,time() +  3600);
            $_SESSION['modulos'][] = "Desarrollo Web en Entorno Servidor";
        }
        if(isset($_POST['deaw'])){
            setcookie("deaw",$_POST['deaw'] ,time() +  3600);
            $_SESSION['modulos'][] = "Despliegue de Aplicaciones Web";
        }
        if(isset($_POST['diw'])){
            setcookie("diw", $_POST['diw'] ,time() +  3600);
            $_SESSION['modulos'][] = "Desarrollo de Interfaces";
        }
        if(isset($_POST['eie'])){
            setcookie("eie", $_POST['eie'] ,time() +  3600);
            $_SESSION['modulos'][] = "Empresa e Iniciativa Emprendedora";
        }
        if(isset($_POST['in'])){
            setcookie("in", $_POST['in'] ,time() +  3600);
            $_SESSION['modulos'][] = "Ingles";
        }

        require "vista4.php";

    }
    //muestro todos los datos
    function mostrarDatos(){
        require "vista5.php";
    }
}

$app = new Sesion();

if (isset($_GET['method'])) {
    $method = $_GET['method'];
} else {
    $method = 'login';
}
if (method_exists($app, $method)) {
    $app->$method();
} else {
    die('metodo no encontrado');
    exit(3);
}